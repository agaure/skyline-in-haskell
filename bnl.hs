import System.IO
import Data.Matrix
import qualified Data.Vector as Vect

data DataTyp = Points | Query
data Win = Win [[Float]] [[Float]] Int deriving (Show)

main = do
        input <- getData Points "sample_cor.txt"
        query:[[winSize]] <- getData Query "sample_query.txt"
        let skylines = beautify (bnl (dimenTrim input query) (Win [] [] (truncate winSize)))
        putStrLn ("No of Skylines : "++  show (length skylines))
        putStr "Skylines : "
        print (skylines)

bnl [] (Win w [] c) = w
bnl [] (Win w t c) = (yesSkyline (Win w t c)) ++ (bnl (noSkyline (Win w t c) ++ t) (Win [] [] (c+ (length w))))
bnl xs (Win w t c) = bnl [] (bnl' xs (Win w t c))

bnl' [] (Win w t c) = Win w t c
bnl' (x:xs) (Win w t c)= bnl' xs (checkWin x (Win w t c))

checkWin x (Win [] t c)
                 | c > 0 = Win [x] t (c-1)
                 | otherwise = Win [] (t++[x]) c
checkWin x (Win (y:w) t c)
                 | isDominate x y = checkWin x (Win w t (c+1))
                 | isDominate y x = Win (y:w) t c
                 | otherwise = wAdd y (checkWin x (Win w t c))

wAdd x (Win w t c) = Win (x:w) t c

isSkyline :: (Ord a) => [a] -> [[a]] -> Bool
isSkyline x [y]
           |isDominate y x = False
           | otherwise = True
isSkyline x (y:ys)
           |isDominate y x = False
           |otherwise = isSkyline x ys

isDominate (x:xs) (y:ys) = isStrictLessAny xs ys && isLessOrEqualAll xs ys

isLessOrEqualAll [x] [y] = x <= y
isLessOrEqualAll (x:xs) (y:ys)
                 | x <= y = isLessOrEqualAll xs ys
                 | otherwise = False

isStrictLessAny [x] [y] = x < y
isStrictLessAny (x:xs) (y:ys)
                 | x < y = True
                 | otherwise = isStrictLessAny xs ys

readInput x = do
               contents <- readFile x
               let rows = lines contents
               return [[read y + 0.0 | y <- words x]| x <- rows]

getData Points x = do
                  input' <- readInput x
                  return(tail input')
getData Query x = do
                    (query':win) <- readInput x
                    return ([map (+1) query'] ++ win)


dimenTrim m q = toLists (transpose (fromLists [Vect.toList (getCol (truncate x) (fromLists m)) |x<- ([1]++q)]))


beautify xs = [truncate (head x)| x<-xs]

yesSkyline (Win [] t c)= []
yesSkyline (Win (x:w) (y:t) c)
                  | (head x) < (head y) = [x] ++ (yesSkyline (Win (w) (y:t) c))
                  | otherwise = yesSkyline (Win (w) (y:t) c)


noSkyline (Win [] t c)= []
noSkyline (Win (x:w) (y:t) c)
                   | (head x) > (head y) = [x] ++ (noSkyline (Win (w) (y:t) c))
                   | otherwise = noSkyline (Win (w) (y:t) c)
