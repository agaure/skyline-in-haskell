import System.IO
import Data.Matrix
import qualified Data.Vector as Vect

data DataTyp = Points | Query
data E = A | B
main = do
        input <- getData Points "sample_cor.txt"
        query <- getData Query "sample_query.txt"
        --print (dimenTrim input query)
        let skylines = naive (dimenTrim input query)
        putStrLn ("Skyline size: "++  show (length skylines))
        putStr "Skylines : "
        print (skylines)

naive xs = naive' xs xs

naive' :: (RealFrac a, Integral b) => [[a]] -> [[a]] -> [b]
naive' [] ys = []
naive' (x:xs) ys
           |isSkyline x ys = [truncate (head x)] ++ naive' xs ys
           |otherwise = naive' xs ys

isSkyline :: (Ord a) => [a] -> [[a]] -> Bool
isSkyline x [y]
           |isDominate y x = False
           | otherwise = True
isSkyline x (y:ys)
           |isDominate y x = False
           |otherwise = isSkyline x ys

isDominate (x:xs) (y:ys) = isStrictLessAny xs ys && isLessOrEqualAll xs ys

isLessOrEqualAll [x] [y] = x <= y
isLessOrEqualAll (x:xs) (y:ys)
                 | x <= y = isLessOrEqualAll xs ys
                 | otherwise = False

isStrictLessAny [x] [y] = x < y
isStrictLessAny (x:xs) (y:ys)
                 | x < y = True
                 | otherwise = isStrictLessAny xs ys

readInput x = do
               contents <- readFile x
               let rows = lines contents
               return [[read y + 0.0 | y <- words x]| x <- rows]

getData Points x = do
                  input' <- readInput x
                  return(tail input')
getData Query x = do
                    (query':win) <- readInput x
                    return [map (+1) query']


dimenTrim m (q:qs) = toLists (transpose (fromLists [Vect.toList (getCol (truncate x) (fromLists m)) |x<- ([1]++q)]))
